
#include "math.hpp"

#include <iostream>
#include <fstream>
#include <iterator>

int
main(int argc, char* argv[]) 
{
  Expr *e = new Add(
    new Int(5),
    new Sub(
      new Int(4),
      new Int(3))
    );
  e->print();
  std::cout << " == " << e->eval() << '\n';
}
