
#include <iostream>
#include <iomanip>
#include <vector>

struct Value
{
  enum Kind {
    boolean,
    integer,
    floating
  };
  union Data {
    bool b;
    int z;
    float f;
  };

  Value(bool b)
    : kind(boolean)
  { data.b = b; }

  Value(int b)
    : kind(integer)
  { data.b = b; }

  Value(float b)
    : kind(floating)
  { data.b = b; }

  Kind kind;
  Data data;
};


// The abstract syntax of the language.
//
// e ::= 0 | 1 | ... | n -- integers
//       0.0 | 3.1 | ... -- floats
//       true | false    -- booleans
//       e1 + e2
//       e1 - e2
//       e1 * e2
//       e1 / e2
//       e1 % e2
//       e1 == e2
//       e1 != e2
//       e1 < e2
//       e1 > e2
//       e1 <= e2
//       e1 >= e2
//       e1 and e2
//       e1 or e2
//       not e1
//
// This is a recursive data structure, which means that
// we can use OOP to do stuff with it.
struct Expr
{
  virtual ~Expr() = default;

  virtual void print() const = 0;
  virtual Value eval() const = 0;
};


// 0 | 1 | ... | n -- integers
struct Int : Expr
{
  Int(int n) : val(n) { }

  void print() const override { 
    std::cout << val; 
  }

  Value eval() const override {
    return Value(val); 
  }

  int val;
};

// true | false -- booleans
struct Bool : Expr
{
  Bool(bool b) : val(b) { }

  void print() const override { 
    std::cout << (val ? "true" : "false"); 
  }

  Value eval() const override {
    return Value(val); 
  }

  bool val;
};

struct Float : Expr
{
  Float(double n) : val(n) { }

  void print() const override {
    std::cout << val;
  }

  Value eval() const override {
    return Value(val);
  }

  double val;
};

// Unary expressions.
struct Unary : Expr
{
  Unary(Expr* e1) : e1(e1) { }
  Expr* e1;
  Expr* e2;
};

// Binary expressions.
struct Binary : Expr
{
  Binary(Expr* e1, Expr* e2) : e1(e1), e2(e2) { }

  void print_expr(const char* op) const { 
    std::cout << '(';
    e1->print();
    std::cout << ' ' << op << ' ';
    e2->print();
    std::cout << ')';
  }


  Expr* e1;
  Expr* e2;
};

// e1 + e2
struct Add : Binary
{
  using Binary::Binary;

  void print() const override { 
    print_expr("+");
  }

  int eval() const override {
    Value v1 = e1->eval();
    Value v2 = e2->eval();
    return v1 + v2;

    // FIXME: Left to the reader as an exercise.
  }
};

// e1 - e2
struct Sub : Binary
{
  using Binary::Binary;

  void print() const override { 
    print_expr("-");
  }

  int eval() const override {
    return e1->eval() - e2->eval();
  }
};

// e1 * e2
struct Mul : Binary
{
  using Binary::Binary;

  void print() const override { 
    print_expr("*");
  }

  int eval() const override {
    return e1->eval() * e2->eval();
  }
};

// e1 / e2
struct Quo : Binary
{
  using Binary::Binary;
};

// e1 % e2
struct Rem : Binary
{
  using Binary::Binary;
};

// -e
struct Neg : Unary
{
  using Unary::Unary;
};

// +e
struct Pos : Unary
{
  using Unary::Unary;
};

// e1 % e2
struct And : Binary
{
  using Binary::Binary;

  Value eval() const override {
    Value v1 = e1->eval();
    Value v2 = e2->eval();
    
    // FIXME: Only define this for boolean values.
  }
};

// e1 % e2
struct Or : Binary
{
  using Binary::Binary;
};


// not e1
struct Not : Unary
{
  using Unary::Unary;
};
