
project(war CXX)
cmake_minimum_required(VERSION 3.5)

set(CMAKE_CXX_FLAGS "-std=c++1z")

# List the set of libraries needed. Options are:
#
#   sfml-window -- Windows and events. Basic application support.
#   sfml-graphics -- 2D drawing
#   sfml-audio -- What it sounds like
#   sfml-network -- Support for computer networking
#   sfml-system -- Data structures, threads, timers, and streams.
#
# Our example needs only windows and graphics.
set(SFML_LIBS sfml-window sfml-graphics sfml-system)

add_executable(toy main.cpp)
target_link_libraries(toy ${SFML_LIBS})

