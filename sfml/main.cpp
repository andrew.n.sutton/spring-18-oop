#include <SFML/Graphics.hpp>

#include <iostream>

class Ball : public sf::CircleShape
{
public:
  Ball()
    : sf::CircleShape(10), m_velocity(1, 1)
  { 
    setFillColor(sf::Color::Red);
  }

  void update()
  {
    sf::Vector2f pos = getPosition();
    if (pos.x < 0 || pos.x > 640)
      m_velocity.x *= -1;
    if (pos.y < 0 || pos.y > 480)
      m_velocity.y *= -1;
    move(m_velocity);
  }
  
private:
  sf::Vector2f m_velocity;
};

int main()
{
  sf::RenderWindow window(sf::VideoMode(640, 480), "Toy example");

  std::vector<Ball*> shapes;

  // Creates a circle with radius 100
  Ball* b0 = new Ball();
  shapes.push_back(b0);

  // Main loop.
  while (window.isOpen()) {
    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed)
        window.close();
      if (event.type == sf::Event::MouseButtonPressed) {
        if (event.mouseButton.button == sf::Mouse::Left) {
          Ball* b = new Ball();
          shapes.push_back(b);
        }
      }
    }

    for (Ball* s : shapes)
      s->update();

    window.clear();
    
    for (Ball* s : shapes)
      window.draw(*s);
    
    window.display();
  }

  return 0;
}