

#include <iostream>
#include <iomanip>
#include <map>
#include <vector>

// Abstractly, represents a set of values.
//
// An example of a JSON object is:
//
//    {
//      "x" : 3,
//      "y" : true,
//      "z" : null,
//      "array" : [0, 1, [1, 2], {}, []],
//      "objects" : { 
//        "foo" : "bar" 
//      }
//    }
//
// This is an object containing a number of sub-objects.
struct Value
{
  // A virtual destructor means that, when deleting an object of this type,
  // the destructor of the object's dynamic type will be called.
  //
  // In general, if a class has any virtual functions, it should have a
  // virtual destructor.
  virtual ~Value() { }

  virtual void print() const = 0;
};

// Represents the value 'null'.
struct Null : Value
{
  Null()
  { }

  void print() const { 
    std::cout << "null"; 
  }
};

// Represents the boolean values 'true' and 'false'.
struct Bool : Value
{
  Bool(bool b)
    : val(b)
  { }

  void print() const { 
    if (val)
      std::cout << "true";
    else
      std::cout << "false";
  }
  
  bool val;
};

/// Represents numeric (floating point values).
struct Number : Value
{
  Number(double d)
    : val(d)
  { }

  void print() const override {
    std::cout << val;
  }

  double val;
};

/// Represents string values.
struct String : Value
{
  String(const std::string& s)
    : str(s)
  { }

  String(const char* first, const char* limit)
    : str(first, limit)
  { }

  void print() const { 
    std::cout << str; 
  }
  
  std::string str;
};

/// Recursive part of data structure.
struct Array : Value
{
  // Constructs an empty array.
  Array() = default;

  // A list constructor. Allows a vector to be constructed by a list 
  // of arguments. For example:
  //
  //    Array a {
  //      new Bool(true), new Null(), ...
  //    };
  Array(std::initializer_list<Value*> list)
    : vals(list)
  { }

  ~Array() override { 
    for (Value* v : vals)
      delete v;
  }

  void add(Value* v) {
    vals.push_back(v);
  }

  void print() const override {
    std::cout << '[';
    for (auto iter = vals.begin(); iter != vals.end(); ++iter) {
      (*iter)->print();
      if (std::next(iter) != vals.end())
        std::cout << ", ";
    }
    std::cout << "]";
  } 
  
  std::vector<Value*> vals;
};

/// 
struct Object : Value
{
  Object() = default;

  ~Object() {
    for (auto p : fields) {
      delete p.first;
      delete p.second;
    }
  }

  void print() const override {
    std::cout << '{';
    for (auto iter = fields.begin(); iter != fields.end(); ++iter) {
      iter->first->print();
      std::cout << " : ";
      iter->second->print();
      if (std::next(iter) != fields.end())
        std::cout << ", ";
    }
    std::cout << "}";
  }

  void add(String* k, Value* v) {
    fields.emplace(k, v);
  }
  
  // FIXME: Compare string values indirectly.
  std::map<String*, Value*> fields;
};


Value* parse(const std::string& str);


