
#pragma once

#include <cassert>
#include <vector>
#include <iosfwd>

// Defines a new type (user-defined type). Sometimes called
// a kind of type constructor (ignore that).
enum Rank {
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

// Represents the suits of a card.
enum Suit {
  Spades,
  Clubs, 
  Diamonds,
  Hearts,
};

enum Color {
  Black, Red
};


class StandardCard {
public:
  StandardCard() = default;

  StandardCard(Rank r, Suit s)
    : rank(r), suit(s)
  { }

  Rank get_rank() const {
    return rank;
  }

  Suit get_suit() const {
    return suit;
  }

private:
  Rank rank;
  Suit suit;
};


class JokerCard
{
public:
  JokerCard(Color c)
    : color(c)
  { }

  Color get_color() const {
    return color;
  }

private:
  Color color;
};


class Card
{
public:
  enum Kind {
    Standard,
    Joker
  };

private:
  union Value {
    Value(StandardCard c) : sc(c) { }
    Value(JokerCard c) : jc(c) { }
    
    StandardCard sc;
    JokerCard jc;
  };

public:

  // Creates a standard card. This establishes an
  // *invariant*. The invariant or property being
  // established is that when kind == Standard, the
  // active member of value is sc.
  //
  // Plural of invariant is invariants. This is not
  // the same as invariance. Do not confuse these.
  Card(StandardCard c)
    : kind(Standard), val(c)
  { }

  // invariant: When kind == Joker, the active member
  // of val is jc.
  Card(JokerCard c)
    : kind(Joker), val(c)
  { }

  Kind get_kind() const { return kind; }
  bool is_standard() const { return kind == Standard; }
  bool is_joker() const { return kind == Joker; }

  StandardCard get_standard_card() const {
    // Do not access a member of a union without
    // first asserting that the kind matches the
    // active member.
    assert(is_standard());
    return val.sc;
  }
  
  JokerCard get_joker_card() const {
    assert(is_joker());
    return val.jc;
  }


  Suit get_suit() const {
    return get_standard_card().get_suit();
  }

  Rank get_rank() const {
    return get_standard_card().get_rank();
  }

  Color get_color() const {
    return get_joker_card().get_color();
  }

private:
  Kind kind;
  Value val;
};

