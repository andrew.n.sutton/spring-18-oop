
#include "card.hpp"

#include <iostream>

void main2();

int main()
{

  StandardCard c0(0, Ace, Spades);
  StandardCard c1(1, Two, Spades);

  std::cout << c0.id << '\n';
  std::cout << c1.get_id() << '\n';

  // This will compile. What does it do?
  // We've converted c0 to a Card.
  //
  // This does not do what you want. This
  // is *object slicing*. You take only
  // the bits in the base class and discard
  // the remainder.
  Card c = c0;

  std::cout << c0.get_rank() << '\n';

  // Does this do what you think?
  // Doesn't compile.
  // std::cout << c.get_rank();

  // Prints 0.
  std::cout << c.get_id() << '\n';

  main2();
}


// Define algorithms on base classes; the
// derived classes may contain specialized
// information not used by the algorithm.
void f(Card& c)
{
  std::cout << c.get_id() << '\n';
}

void main2() {

  StandardCard* foo = new StandardCard(0, Ace, Spades);

  StandardCard c0(0, Ace, Spades);
  JokerCard j0(52, Red);

  Card& c = c0;
  // Card is the static type of c.
  // The dynamic type of c is StandardCard.

  // Can't do this.
  // JokerCard& j = c;

  // c.rank == Ace; // error: no such member

  f(c0);
  f(j0);

  // Pointers can be widened from derived
  // to base classes.
  Card* p = &c0;

  // Not allowed. Cannot implicitly narrow
  // a base class to a derived class.
  // StandardCard* p1 = p;
}






