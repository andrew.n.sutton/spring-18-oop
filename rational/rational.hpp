#pragma once

#include <iostream>

#include <cassert>

class rational
{
public:
  // Does this construct a rational number?
  //
  // No. If d == 0, you would construct n / d, which
  // is not a number.
  //
  // precondition: d != 0
  //
  // It is *ALWAYS* the caller's (user's) responsibility
  // to satisfy a precondition.
  rational(int n, int d)
    : num(n), den(d)
  {
    // This is morally reprehensible. Do not do this. This
    // silently accepts the error (even though it prints)
    // and pretends the program is okay.
    //
    // The program still has undefined behavior, but this
    // is not useful.
    // if (d == 0)
    //   std::cerr << "error\n";

    // This is good. Assertions are only triggered in debug
    // mode. If you compile in release mode, the assertions
    // are removed from your code.
    assert(d != 0);

    // This is good too, but it changes the contract.
    // if (d == 0)
    //   throw std::logic_error("invalid denominator");
  }

private:
  int num; // numerator
  int den; // denominator
};
