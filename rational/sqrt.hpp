
#pragma once

// Precondition: n >= 0
// Postcondition r: r * r = n;
double sqrt(double n);

/// A restricted floating point value.
///
/// This class restricts the set of values of
/// floating point numbers by ensuring that it
/// can not be constructed with an value less
/// than zero.
class nonnegative
{
  nonnegative(double n)
    : m_val(n)
  {
    assert(n >= 0);
  }

  double m_val;
};


// Total function: maps every input to an output.
// Total functions are great. Why?
//
// Total functions (or those without preconditions)
// NEVER fail. It is not possible for this function
// to be called in a way that produces an error.
//
// And you never have to check for errors.
nonnegative sqrt(nonnegative n);


// If a function has a wide contract, and returns
// an error condition when the inputs are invalid,
// meaning they do not lead to a valid result, then
// the caller MUST check the error condition.

// Wide contracts produce programs that look
// like this:

int fd = open("some file", O_RDONLY);
if (fd < 0)
  // error.
int ret = write(fd, ...);
if (ret < 0)
  // error.

double n = sqrt(x);
if (is_nan(n))
  // error.

// Do something, check the result.

// On the other hand, narrow contracts do this:

if (n <= 0)
  // error
double x = sqrt(n);

// Check the preconditions, then do something.

// ---------------

// Type safe programming. An ideal. Allows for
// straightline code.

nonnegative n = ...;
nonnegative x = sqrt(n);
nonnegative y = sqrt(y);

// Correct by construction.

// Exceptions also allow for correct by construction
// code. Except that somewhere, somehow, an exception
// can be caught and processed.




