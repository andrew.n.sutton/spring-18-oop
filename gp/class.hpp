#pragma once

template<typename T>
class list_node
{
  list_node<T>* next;
  list_node<T>* prev;
  T data;
};

template<typename T>
class list
{
  list_node<T>* head;
  list_node<T>* tail;
};


// Instantiate the template:
list<int> x;


// Implicitly creates this specialization:
template<>
class list<int>
{
  list_node<int>* head;
  list_node<int>* tail;
};

template<>
class list_node<int>
{
  list_node<int>* next;
  list_node<int>* prev;
  int data;
};


// -------------------------------//

template<typename T>
class vector
{
  T& operator[](int n) {
    return *(first + n);
  }
private:
  T* first;
  T* last;
  T* end;
};

vector<int> v(5);
v[0] = 4;

// Optimize storage by using a bitstring
// instead of an array of bits.
template<>
class vector<bool> : boost::dynamic_bitset
{
  bool operator[](int n) {
    return /// boost's operator[].
  }
};


/// This is a partial specialization of
/// a class tempate.
template<typename T>
class vector<T*> 
{

};


vector<int> v1; // I get the generic vector.
vector<bool> v2; // I get the explicit specialization.
vector<int*> v3; // I get the partial spec.
vector<void*> v3; // I get the partial spec.
vector<char*> v3; // I get the partial spec.
vector<float*> v3; // I get the partial spec.
vector<std::string*> v3; // I get the partial spec.
