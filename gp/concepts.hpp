#pragma

template<typename T>
concept bool EqualityComparable =
  requires (T a, T b) {
    { a == b } -> bool;
    // a == b => a equals b
    // a equals if b is a copy of b
    // or vice versa.

    { a != b } -> bool;
    // a != b <=> !(a == b)
  };

template<typename T>
concept bool TotallyOrdered =
  EqualityComparable<T> &&
  requires (T a, T b) {
    { a < b } -> bool;
    // Exactly one of the following
    // are true:
    // a < b
    // b < a
    // a == b (*)
    // Trichotemy law.
    // Not quite "for all a and b"
    
    { a > b } -> bool;
    // a > b <=> b < a

    { a <= b } -> bool;
    // a <= b <=> !(b < a)

    { a >= b } -> bool;
    // a >= b <=> !(a < b)
  };
