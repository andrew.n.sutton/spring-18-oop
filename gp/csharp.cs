
T min<T extends IComparable>(T a, T b)
{
  return a.compareTo(b) < 0 ? a : b;
}


