
def min_value(a, b):
  return a if a < b else b

print min_value(0, 4)
print min_value("abc", "def")
print min_value(0, "abc")

class Rational:
  def __init__(self):
    self.n = 0
    self.d = 1

r = Rational()
print r.__dict__
print Rational.__dict__
