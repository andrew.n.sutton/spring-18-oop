#include "concepts.hpp"

#include <iostream>

template<Ordered T>
  // requires Ordered<T>
T min_value(T a, T b)
{
  return a < b ? a : b;
}

struct Rational 
{
  int n, d;
};

bool operator==(Rational a, Rational b)
{
  return true;
}

bool operator<(Rational a, Rational b)
{
  return false;
}

bool operator>(Rational a, Rational b)
{
  return b < a;
}

bool operator<=(Rational a, Rational b)
{
  return !(b < a);
}

bool operator>=(Rational a, Rational b)
{
  return !(a < b);
}

int main()
{
  min_value(3, 4);

  Rational r1, r2;
  min_value(r1, r2);

  float NaN = 0.0 / 0.0;
  std::cout << NaN << '\n';
  cout << (NaN < NaN) << '\n';
  cout << (NaN > NaN) << '\n';
  cout << (NaN == NaN) << '\n';

  float f1 = NaN;
  float f2 = NaN;
  cout << 
    (min_value(f1, f2) == min_value(f2, f1)) << '\n';
}

template<typename T, int N>
class array
{
private:
  T data[N];
};

array<float, 3> vec3;




