#pragma once

// I want to do this:
// is_const<const int>
// is_const<int>

template<typename T>
struct is_const_trait
{
  static constexpr bool value = false;
};

template<typename T>
struct is_const_trait<const T>
{
  static constexpr bool value = false;
};

template<typename T>
constexpr int is_const = is_const_trait<T>::value;

is_const<int> // false
is_const<const int> // true



is_same<int, int>
is_same<int, false>