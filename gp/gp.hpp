#pragma once

// Generic version of an algorithm.
// A generic algorithm.
//
template<typename T> // Declares a placeholder type T
T min_value(T a, T b)
{
  return a < b ? a : b;
}
// T is also called a template parameter.
// We need to provide arguments for this later.
// See below.

// An overload of min_value. This is provides
// a specialization for the algorithm that
// accepts references (to variables).
template<typename T>
T& min_value(T& a, T& b)
{
  return a < b ? a : b; 
}

// Yet another overload. This is a kind of
// partial specialization... It's "partial"
// because it's not defined in terms of
// concrete types.
template<typename T>
vector<T>& min_value(vector<T>& a, vector<T>& b);


void f() {
  // Explicitly specify the type arguments
  // (or *template arguments*) to the generic
  // algorithm.
  min_value<int>(0, 3);
  // This is instantiates the template min.
  // Instantiation substitutes template arguments
  // for their corresponding template parameters
  // and produces a NEW declaration.
  // See below.

  // int a, b;
  // min<int>(a, b);

  // With functions, you don't have to explicitly
  // specify the template arguments. The
  // compiler will figure it for you. This process
  // is called "template argument deduction".
  min(0, 3); // Same min<int>(0, 3)
}

// The compiler actually generates something
// like this:
template<>
double min_value<double>(double a, double b)
{
  return a < b ? a : b;
}
// This is called a template specialization.
// This one has been generated implicitly by
// the instantiation above.
//
// Note that we can explicitly write this in our
// code in order to supply more efficient versions
// of the algorithm. This is called an explicit
// specialization. The supplied template argument
// must be a concrete type.
// For example:
template<>
int min_value<int>(int a, int b)
{
  return y ^ ((x ^ y) & -(x < y));
}
// Note that we are substituting this version of
// the function for the more generic one. It
// had better satisfy the same requirements as
// the more generic one.



// Object-oriented version.
// Not a good solution.

class Comparable
{
  virtual bool less(Comparable& that) = 0;
};

Comparable& min(Comparable& a, Comparable& b)
{
  if (a.less(b))
    return a;
  else
    return b;
}
